import numpy as np


class Network:
    def __init__(self, sizes):
        # Liczba warstw
        self._layers_count = len(sizes)
        # Rozmiary kolejnych warstw sieci
        self._sizes = sizes
        # Losowe biasy kolejnych warstw
        self._biases = [np.random.randn(y, 1) for y in sizes[1:]]
        # Wagi neuronów
        self._weights = [np.random.randn(y, x)
                         for x, y in zip(sizes[:-1], sizes[1:])]

    @staticmethod
    def sigmoid(z):
        return 1.0 / (1.0 + np.exp(-z))

    @staticmethod
    def sigmoid_prime(z):
        return Network.sigmoid(z) * (1.0 - Network.sigmoid(z))

    @staticmethod
    def cost_derivative(output_activations, y):
        return (output_activations - y)

    def feedforward(self, a):
        for b, w in zip(self._biases, self._weights):
            a = Network.sigmoid(np.dot(w, a) + b)
        return a

    def backprop(self, x, y):
        # Gradienty funkcji kosztu
        nabla_b = [np.zeros(b.shape) for b in self._biases]
        nabla_w = [np.zeros(w.shape) for w in self._weights]

        # Forward
        activation = x
        activations = [x]
        zs = []
        for b, w in zip(self._biases, self._weights):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = Network.sigmoid(z)
            activations.append(activation)

        # Backward
        delta = self.cost_derivative(activations[-1], y)\
                * Network.sigmoid_prime(zs[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())

        for l in range(2, self._layers_count):
            z = zs[-l]
            sp = Network.sigmoid_prime(z)
            delta = np.dot(self._weights[-l+1].transpose(), delta) * sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())

        return (nabla_b, nabla_w)


    def update_batch(self, batch, l_rate):
        '''Aktualizacja wartości wag i biasów dla pojedynczej próbki danych'''
        nabla_b = [np.zeros(b.shape) for b in self._biases]
        nabla_w = [np.zeros(w.shape) for w in self._weights]

        for x, y in batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]

        self._weights = [w - (l_rate / len(batch)) * nw
                         for w, nw in zip(self._weights, nabla_w)]

        self._biases = [b - (l_rate / len(batch)) * nb
                        for b, nb in zip(self._biases, nabla_b)]

    def evaluate(self, test_data):
        test_results = [(np.argmax(self.feedforward(x)), y)
                        for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)

    def sgd(self, training_data, epochs, batch_size, l_rate, test_data=None):
        '''Mini-batch stochastic gradient descent'''
        if test_data is not None:
            test_data_count = len(test_data)
        training_data_count = len(training_data)

        for i in range(epochs):
            np.random.shuffle(training_data)
            batches = [training_data[k:k+batch_size]
                       for k in range(0, training_data_count, batch_size)]

            for batch in batches:
                self.update_batch(batch, l_rate)

            if test_data:
                evaluated = self.evaluate(test_data)
                acc = evaluated / test_data_count * 100

                print(f'Epoka {i}: {evaluated} / {test_data_count} ({acc}%)')
            else:
                print(f'Epoka {i} zakończona.')

