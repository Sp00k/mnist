import numpy as np
from PIL import Image

import mnist_loader
from network import Network

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
training_data2, validation_data2, test_data2 = mnist_loader.load_data_wrapper()
net = Network([784, 30, 10])

net.sgd(list(training_data), 5, 10, 3.0, test_data=list(test_data))

test_data_as_list = list(test_data2)
np.random.shuffle(test_data_as_list)
for i in range(len(test_data_as_list)):
    probe = test_data_as_list[i][0] * 255
    print(np.argmax(net.feedforward(probe)))
    img = Image.fromarray(np.reshape(probe, (28,28)).astype(np.uint8), 'L')
    img.show()
    input()
